const mongoose = require("mongoose");

module.exports = async function mongoConnect() {
  // Connect to mongodb server
  await mongoose.connect(
    "mongodb+srv://admin:admin1234@transactions.go6r7.mongodb.net/interview?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    }
  );
};
