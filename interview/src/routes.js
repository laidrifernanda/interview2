const router = require("express").Router();

const UsersModel = require("../models/users")();

// const middlewareAddUsers = require("../middleware/addusers");

module.exports = function routes() {
  router.get("/users", async (req, res) => {
    try {
      const data = await UsersModel.find();
      res.json({ message: "success read data users", data: data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when read data users" });
    }
  });

  router.post("/users", async (req, res) => {
    try {
      await UsersModel.create(req.body);
      res.json({ message: "success create new data users" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when create data users" });
    }
  });

  router.put("/users", async (req, res) => {
    try {
      await UsersModel.update(req.body);
      res.json({ message: "success update data" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when update data users" });
    }
  });

  return router;
};

// router.delete("/users", (req, res) => res.send("hello world"));
