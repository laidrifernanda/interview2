const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = function UsersModel() {
  const usersSchema = new Schema({
    fullName: String,
    jeniskelamin: String,
    age: Number,
  });

  return mongoose.model("users", usersSchema);
};
